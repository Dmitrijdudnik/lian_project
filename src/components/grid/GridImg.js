import React, { Component } from 'react';
import '../../scss/Grid.scss';
import gridImg1 from '../../images/gridImg1.png'
import gridImg2 from '../../images/gridImg2.png'
import gridImg3 from '../../images/gridImg3.png'
import gridImg4 from '../../images/gridImg4.png'
import gridImg5 from '../../images/gridImg5.png'
import gridImg6 from '../../images/gridImg6.png'
import gridImg7 from '../../images/gridImg7.png'
import gridImg8 from '../../images/gridImg8.png'
import gridImg9 from '../../images/gridImg9.png'
import gridImg10 from '../../images/gridImg10.png'

class GridImg extends Component {
  render() {
    return (
      <div className='row'>
        <div className='column'>
            <div className="image-container">
                <img src={gridImg1} alt='img'/>
                <div className="after">+</div>
            </div>
            <div className="image-container">
                <img src={gridImg4} alt='img'/>
                <div className="after">+</div>
            </div>
            <div className="image-container">
                <img src={gridImg7} alt='img'/>
                <div className="after">+</div>
            </div>
        </div>
        <div className='column'>
            <div className="image-container">
                <img src={gridImg2} alt='img'/>
                <div className="after">+</div>
            </div>
            <div className="image-container">
                <img src={gridImg5} alt='img'/>
                <div className="after">+</div>
            </div>
            <div className="image-container">
                <img src={gridImg8} alt='img'/>
                <div className="after">+</div>
            </div>
            <div className="image-container">
                <img src={gridImg10} alt='img'/>
                <div className="after">+</div>
            </div>   
        </div>
        <div className='column third-column'>
            <div className="image-container">
                <img src={gridImg3} alt='img'/>
                <div className="after">+</div>
            </div>
            <div className="image-container">
                <img src={gridImg6} alt='img'/>
                <div className="after">+</div>
            </div>
            <div className="image-container">
                <img src={gridImg9} alt='img'/>
                <div className="after">+</div>
            </div> 
        </div>         
      </div>
    );
  }
}

export default GridImg;
